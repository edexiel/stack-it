# Stack It
Le jeu Stack it! est un jeu d'adresse dont le but est d'empiler le plus de cubes possible

La seule touche pour tout faire est Espace

## Comment compiler et lancer
Se déplacer dans le dossier contenant le projet ( le dossier dans lequel est actuellement ce fichier )
Dans un invite de commande lancer :
``` bash
	$ make && ./StackIt
```

## Ce qui ne marche pas
* La lumière
* Le deltaTime pour les animations
* Le fade In - Fade out avec une image 2D

## Ce qui peut être amélioré
* Utiliser thalès pour calculer la position de la camera en fonction de la taille de la tour
* L'interface graphique pour afficher le score utilise honteusement IM