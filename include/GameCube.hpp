#pragma once

#include "primitive.hpp"
#include <stdlib.h>

class GameCube : public Cube
{
private:
    bool fixed;            // is the cube fixed in the scene
    float travel_distance; //the max distance, relative to x:0 y:0 the cube has to travel
    bool axis;             // true x false y
    float direction;
    float speed;

public:
    GameCube(Vector3f _origin, Vector3f _scale);
    GameCube(Vector3f _origin, Vector3f _scale, bool fixed);
    ~GameCube();
    void update();
    void setFixed(bool);
    bool getAxis();
};
