#pragma once

#include <vector>
#include "GameCube.hpp"
#include "Vector3D.hpp"
#include "camera.hpp"

class InputManager;

class GameLevel
{
private:
    Camera *                camera;
    InputManager *          io;
    Vector3f                pos;
    Vector3f                scale;
    unsigned int            m_score;
    std::vector<GameCube>   cubes;
    bool                    lost;
    float                   lost_anim_rotation;
    float                   diff;

public:
    GameLevel(Camera *, InputManager *);
    ~GameLevel();

    void update();
    void draw();
    void addCube(Vector3f new_origin, Vector3f new_scale);
    void resetLevel();
    unsigned int getScore();
};