#pragma once

#include "Vector2D.hpp"
#include <GLFW/glfw3.h>
#include <map>

//key to be watched and mouse
struct input
{
    std::map<int, bool> keys;
    Vector2d mouse_keys;
    Vector2d mouse;
};

class InputManager
{
private:
    GLFWwindow *m_window;
    input current_input;
    input old_input;

public:
    InputManager(GLFWwindow *);
    ~InputManager();
    void update();
    bool keyUp(int key);
    bool keyDown(int key);
    bool isDown(int key);

    Vector2d getMousePosition();
    Vector2d getMouseOldPosition();
    Vector2d getMouseMovement();
};
