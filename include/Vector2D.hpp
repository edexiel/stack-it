#pragma once

template <typename T>
struct Vector2D
{
    T x;
    T y;
};

typedef Vector2D<float> Vector2f;
typedef Vector2D<double> Vector2d;