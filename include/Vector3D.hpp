#pragma once

template <typename T>
struct Vector3D
{
    T x;
    T y;
    T z;
};

typedef Vector3D<float> Vector3f;
typedef Vector3D<float> Color;