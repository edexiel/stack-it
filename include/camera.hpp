#include "Vector3D.hpp"

#pragma once

struct Camera
{
    Vector3f cameraPos;
    float cameraYaw;
    float cameraPitch;
    float zNear;
    float zFar;
    float fov;
    bool perspective;
    bool enableDepth;
};