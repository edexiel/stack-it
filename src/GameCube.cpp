#include "GameCube.hpp"

#include <iostream>

GameCube::GameCube(Vector3f _origin, Vector3f _scale) : Cube{_origin, _scale}
{
    fixed = true;
    travel_distance = 10;
    bool random = (bool)(std::rand() % 2);
    // bool random = 1;
    speed = 0.15;
    axis = random ? 1 : 0;
    direction = random ? 1 : -1;
}

GameCube::GameCube(Vector3f _origin, Vector3f _scale, bool _fixed) : GameCube(_origin, _scale)
{
    fixed = _fixed;

    if (!fixed)
    {
        if (axis)
            origin.x -= travel_distance;
        else
            origin.z += travel_distance;
    }
}

GameCube::~GameCube() {}

void GameCube::update()
{
    if (fixed)
        return;

    if (axis) //translating on X
    {
        if (origin.x <= -travel_distance)
            direction = 1;
        else if (origin.x >= travel_distance)
            direction = -1;

        origin.x += direction * speed; //*deltaTime
    }
    else //translating on Z
    {
        if (origin.z <= -travel_distance)
            direction = 1;
        else if (origin.z >= travel_distance)
            direction = -1;

        origin.z += direction * speed; //*deltaTime
    }
}

void GameCube::setFixed(bool b)
{
    fixed = b;
}

bool GameCube::getAxis()
{
    return axis;
}
