#include "GameLevel.hpp"
#include "InputManager.hpp"
#include <iostream>
#include <cmath>

GameLevel::GameLevel(Camera *_camera, InputManager *_io) : camera{_camera}, io{_io}, pos{(Vector3f){0, 0.5, 0}}, scale{(Vector3f){5, 1, 5}}
{
    cubes.push_back((GameCube){pos, scale});
    m_score = 0;
    lost = false;
}

GameLevel::~GameLevel() {}

void GameLevel::update()
{
    if (lost)
    {
        if (io->keyDown(GLFW_KEY_SPACE))
        {
            resetLevel();
        }
        return;
    }

    if (io->keyDown(GLFW_KEY_SPACE))
    {
        if (cubes.size() == 1)
        {
            pos = {pos.x, pos.y + scale.y, pos.z};
            addCube(pos, scale);
            return;
        }

        cubes.back().setFixed(true);

        if (cubes.back().getAxis()) // axis is X
        {

            //shape the current cube against current-1;
            diff = cubes.back().getOrigin().x - cubes[cubes.size() - 2].getOrigin().x;

            if (fabs(diff- scale.x / 2) < 0.15f)
                diff = 0;

            scale.x -= std::fabs(diff);

            if (scale.x <= 0)
            {
                lost = true;
                return;
            }

            m_score++;
            cubes.back().setScale(scale);
            pos = {cubes[cubes.size() - 2].getOrigin().x + diff / 2.f, pos.y, pos.z};
        }
        else // axis is Z
        {

            //shape the current cube against current-1;
            diff = cubes.back().getOrigin().z - cubes[cubes.size() - 2].getOrigin().z;

            if (fabs(diff - scale.z / 2) < 0.15f)
                diff = 0;

            scale.z -= std::fabs(diff);

            if (scale.z <= 0)
            {
                lost = true;
                return;
            }

            m_score++;
            cubes.back().setScale(scale);
            pos = {pos.x , pos.y, cubes[cubes.size() - 2].getOrigin().z + diff/ 2.f};
        }

        cubes.back().setOrigin(pos);

        pos = {pos.x, pos.y + scale.y, pos.z};

        addCube({pos.x, pos.y, pos.z}, scale);

        camera->cameraPos.y += scale.y;
    }

    for (GameCube &c : cubes)
    {
        c.update();
    }
}

void GameLevel::draw()
{
    if (lost)
    {
        glPushMatrix();
        glRotatef(lost_anim_rotation, 0, 1, 0);

        for (GameCube &c : cubes)
        {
            c.draw();
        }

        glPopMatrix();

        lost_anim_rotation = fmod((lost_anim_rotation + 0.1f), 360); // * deltatime
    }
    else
    {
        for (GameCube &c : cubes)
        {
            c.draw();
        }
    }
}

void GameLevel::addCube(Vector3f new_origin, Vector3f new_scale)
{
    GameCube c{new_origin, new_scale, false};

    c.setColor({(rand() % 100) / 100.f, (rand() % 100) / 100.f, (rand() % 100) / 100.f});
    cubes.push_back(c);
}

void GameLevel::resetLevel()
{
    camera->cameraPos.y -= (cubes.size() - 2) * scale.y;
    cubes.clear();

    pos = (Vector3f){0, 0.5, 0};
    scale = (Vector3f){5, 1, 5};

    m_score = 0;
    lost_anim_rotation = 0;
    cubes.push_back((GameCube){pos, scale});
    lost = false;
}

unsigned int GameLevel::getScore()
{
    return m_score;
}