#include "InputManager.hpp"

InputManager::InputManager(GLFWwindow *window) : m_window{window}
{
    current_input.keys[GLFW_KEY_SPACE] = false;
}

InputManager::~InputManager() {}

void InputManager::update()
{
    old_input = current_input;

    // Keyboard
    for (const auto &myPair : current_input.keys)
        current_input.keys[myPair.first] = (bool)glfwGetKey(m_window, myPair.first);

    //Mouse keys
    current_input.mouse_keys.x = (bool)glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_LEFT);
    current_input.mouse_keys.y = (bool)glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_RIGHT);
    //Mouse
    glfwGetCursorPos(m_window, &current_input.mouse.x, &current_input.mouse.y);
}

bool InputManager::keyUp(int key)
{
    return old_input.keys[key] && !current_input.keys[key];
}

bool InputManager::keyDown(int key)
{
    return current_input.keys[key] && !old_input.keys[key];
}

bool InputManager::isDown(int key)
{
    return current_input.keys[key];
}

Vector2d InputManager::getMousePosition()
{
    return current_input.mouse;
}

Vector2d InputManager::getMouseOldPosition()
{
    return old_input.mouse;
}

Vector2d InputManager::getMouseMovement()
{
    return {current_input.mouse.x - old_input.mouse.x,current_input.mouse.y - old_input.mouse.y};
}
