#include <cstdio>
#include <cmath>
#include <cstdlib>

#include "glad/glad.h"
#include <GLFW/glfw3.h>

#include <GL/glu.h>

#include "im_api.h"

#include "camera.hpp"
#include "GameLevel.hpp"
#include "InputManager.hpp"
#include <ctime>

#define DEBUG 0

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void setupGLForUI(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, (GLdouble)width, height, (GLdouble)0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_BLEND);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);
}

void setupGLForDrawing(Camera &cam, int screenWidth, int screenHeight)
{
    float aspect = (float)screenWidth / screenHeight;

    // Parameters UI
    {
#if DEBUG
        im_checkbox("Perspective", &cam.perspective);
        if (cam.perspective)
        {
            im_sliderFloat("fov", &cam.fov, 20.f, 180.f);
            im_sliderFloat("zNear", &cam.zNear, 0.01f, 1.f);
            im_sliderFloat("zFar", &cam.zFar, 0.01f, 200.f);
        }

        //player cam setting
        im_text("camera : ");

        //global cam settings

        // im_checkbox("Look at mode ( not impleted ) :", &cam.cameraLookat);
        im_sliderFloat("camera.x", &cam.cameraPos.x, -10.f, 15.f);
        im_sliderFloat("camera.y", &cam.cameraPos.y, -10.f, 50.f);
        im_sliderFloat("camera.z", &cam.cameraPos.z, -10.f, 15.f);
        im_sliderFloat("camera pitch", &cam.cameraPitch, -90.f, 90.f);
        im_sliderFloat("camera yaw", &cam.cameraYaw, -180.f, 180.f);
        // im_checkbox("Draw Player model :", &cam.drawPlayerModel);

        im_checkbox(cam.enableDepth ? "Depth test (enabled)" : "Depth test (disabled)", &cam.enableDepth);
#endif
    }

    // Set up projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (cam.perspective)
        gluPerspective(cam.fov, aspect, cam.zNear, cam.zFar);
    else
        gluOrtho2D(-aspect, aspect, 1, -1);

    // Set up model-view
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(cam.cameraPitch, -1, 0, 0);
    glRotatef(cam.cameraYaw, 0, -1, 0);
    glTranslatef(-cam.cameraPos.x, -cam.cameraPos.y, -cam.cameraPos.z);

    // if (cam.cameraLookat)
    // {
    //     gluLookAt();
    // }

    // Enable/Disable capabilities
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);

    if (cam.enableDepth)
        glEnable(GL_DEPTH_TEST);
}

int main(int argc, char *argv[])
{
    int screenWidth = 1280;
    int screenHeight = 720;
    double old_time = glfwGetTime();
    double delta_time;

    Camera camera{
        {10, 15, -10},
        135,
        -45,
        0.01,
        50,
        60,
        true,
        true,
    };

    // Init GLFW
    if (!glfwInit())
    {
        fprintf(stderr, "glfwInit failed.\n");
        return -1;
    }

    // Tell GLFW to create a OpenGL 3.1 context on next glfwCreateWindow() call
    // 3.1 is the smallest version supported on this machine
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    // Create window
    GLFWwindow *window = glfwCreateWindow(screenWidth, screenHeight, "Stack it!", NULL, NULL);
    if (!window)
    {
        fprintf(stderr, "glfwCreateWindow failed.\n");
        glfwTerminate();
        return -1;
    }

    glfwSwapInterval(1); // Enable v-sync

    // We make the OpenGL context current on this calling thread for the window
    // It's mandatory before any OpenGL call
    glfwMakeContextCurrent(window);

    // Load OpenGL functions
    if (!gladLoadGL())
    {
        fprintf(stderr, "gladLoadGL failed.\n");
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback(window, key_callback);

    // Load gui library
    im_init();
    im_setItemWidth(270.f);

    std::srand(std::time(nullptr));
    //Serious business
    InputManager io(window);
    GameLevel gm(&camera, &io);
    Triedre t{{0, 0, 0}};

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        io.update();

        // Resize viewport
        glfwGetWindowSize(window, &screenWidth, &screenHeight);
        glViewport(0, 0, screenWidth, screenHeight);

        glClearColor(0.5, 0.5, 0.5, 1);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Show UI
        {
            setupGLForUI(screenWidth, screenHeight);
#if DEBUG
            im_io_t io;
            io.mouseLeft = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS;
            double mouseX, mouseY;
            glfwGetCursorPos(window, &mouseX, &mouseY);
            io.mousePos[0] = mouseX;
            io.mousePos[1] = mouseY;

            im_newFrame(io);
            // Display OpenGL information
            im_text("GL_VENDOR: %s", glGetString(GL_VENDOR));
            im_text("GL_RENDERER: %s", glGetString(GL_RENDERER));
            im_text("GL_VERSION: %s", glGetString(GL_VERSION));
#endif
        }
        // Pas eu le temps de le faire proprement
        im_io_t io_b;
        io_b.mouseLeft = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS;
        double mouseX, mouseY;
        glfwGetCursorPos(window, &mouseX, &mouseY);
        io_b.mousePos[0] = mouseX;
        io_b.mousePos[1] = mouseY;
        im_newFrame(io_b);

        im_text("Score: %u", gm.getScore());

        // if(light)
        // glEnable(GL_LIGHTING)


        // game logic
        gm.update();

        // Draw
        {
            setupGLForDrawing(camera, screenWidth, screenWidth);
            gm.draw();
            t.draw();
        }

        // Present buffer
        glfwSwapBuffers(window);
    }

    im_shutdown();
    glfwTerminate();

    return 0;
}
