#include "glad/glad.h"
#include "primitive.hpp"
#include <cmath>

using namespace std;

//Shape
Shape::Shape() {}
Shape::Shape(Vector3f _origin, short draw_mode) : m_draw_mode{draw_mode}, origin{_origin}, m_color{1, 1, 1} {}
Shape::~Shape()
{
    points.clear();
}

void Shape::setDrawMode(short draw_mode)
{
    m_draw_mode = draw_mode;
}

void Shape::setColor(Color c)
{
    m_color = c;
}

void Shape::setScale(Vector3f scale)
{
    m_scale = scale;
}

void Shape::setOrigin(Vector3f _origin)
{
    origin = _origin;
}


// void Shape::move(Vector3f &p, float divider)
// {
//     origin.x += p.x / divider;
//     origin.y += p.y / divider;
//     origin.z += p.z / divider;
// }

Vector3f &Shape::getOrigin()
{
    return origin;
}

//Sphere
Sphere::Sphere(Vector3f _origin, unsigned int l, unsigned int L) : Shape{_origin, GL_TRIANGLE_STRIP}
{
    m_L = L;
    m_l = l;

    for (size_t i = 0; i < l; i++)
    {
        for (size_t j = 0; j < L + 1; j++)
        {
            points.push_back(sp(i, j, l, L));
        }
    }

    points.push_back({0, -1, 0});
}

Vector3f Sphere::sp(unsigned int i, unsigned int j, unsigned int l, unsigned int L)
{
    float y = cosf(i * M_PI / l);
    float r = sin(i * M_PI / l);
    float x = cosf(j * (M_PI * 2) / L) * r;
    float z = sinf(j * (M_PI * 2) / L) * r;

    return {x, y, z};
}

Sphere::~Sphere() {}

void Sphere::draw()
{
    glBegin(m_draw_mode);

    float mod_color = 0;
    unsigned int i_max = points.size() - 1;
    for (unsigned int i = 0; i < points.size(); i++)
    {
        int index = i + m_L + 1 > i_max ? i_max : i + m_L + 1;

        glColor3f(m_color.x - mod_color, m_color.y - mod_color, m_color.z - mod_color);
        glVertex3f(points[i].x + origin.x, points[i].y + origin.y, points[i].z + origin.z);
        glVertex3f(points[index].x + origin.x, points[index].y + origin.y, points[index].z + origin.z);

        mod_color += (1 / (float)i_max);
    }

    glEnd();
    glFlush();
}

Triedre::Triedre(Vector3f _origin) : Shape{_origin, GL_LINES}, cone{_origin, 50}
{
    points.push_back({_origin.x + 1, 0, 0});
    points.push_back({0, _origin.y + 1, 0});
    points.push_back({0, 0, _origin.z + 1});
}

Triedre::~Triedre() {}

void Triedre::draw()
{
    glPushMatrix();
    glRotatef(-90, 0, 0, 1);
    glTranslatef(0, 1, 0);
    glScalef(0.05, 0.15, 0.05);
    cone.setColor({1, 0, 0});
    cone.draw();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, 1, 0);
    glScalef(0.05, 0.15, 0.05);
    cone.setColor({0, 1, 0});
    cone.draw();
    glPopMatrix();

    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(0, 1, 0);
    glScalef(0.05, 0.15, 0.05);
    cone.setColor({0, 0, 1});
    cone.draw();
    glPopMatrix();

    glBegin(m_draw_mode);

    glColor3f(1, 0, 0);
    glVertex3f(origin.x, origin.y, origin.z);
    glVertex3f(points[0].x, points[0].y, points[0].z);
    glColor3f(0, 1, 0);
    glVertex3f(origin.x, origin.y, origin.z);
    glVertex3f(points[1].x, points[1].y, points[1].z);
    glColor3f(0, 0, 1);
    glVertex3f(origin.x, origin.y, origin.z);
    glVertex3f(points[2].x, points[2].y, points[2].z);

    glEnd();
    glFlush();
}

Cube::Cube(Vector3f _origin) : Shape{_origin, GL_TRIANGLE_STRIP}
{
    points.push_back({-0.5, -0.5, 0.5});
    points.push_back({0.5, -0.5, 0.5});
    points.push_back({-0.5, -0.5, -0.5});
    points.push_back({0.5, -0.5, -0.5});
    points.push_back({0.5, 0.5, -0.5});
    points.push_back({0.5, -0.5, 0.5});
    points.push_back({0.5, 0.5, 0.5});
    points.push_back({-0.5, -0.5, 0.5});
    points.push_back({-0.5, 0.5, 0.5});
    points.push_back({-0.5, -0.5, -0.5});
    points.push_back({-0.5, 0.5, -0.5});
    points.push_back({0.5, 0.5, -0.5});
    points.push_back({-0.5, 0.5, 0.5});
    points.push_back({0.5, 0.5, 0.5});
}

Cube::Cube(Vector3f _origin, Vector3f scale) : Cube(_origin)
{
    m_scale = scale;
}

Cube::~Cube() {}

void Cube::draw()
{
    glPushMatrix();

    glTranslatef(origin.x, origin.y, origin.z);
    glScalef(m_scale.x, m_scale.y, m_scale.z);

    glBegin(m_draw_mode);

    for (Vector3f &p : points)
    {
        glColor3f(m_color.x, m_color.y, m_color.z);
        glVertex3f(p.x, p.y, p.z);
    }

    glEnd();
    glFlush();

    glPopMatrix();
}

Cone::Cone(Vector3f _origin, unsigned int l) : Shape{_origin, GL_TRIANGLE_STRIP}
{
    for (size_t i = 0; i < l + 1; i++)
    {
        points.push_back({cosf(i * (M_PI * 2) / l), 0, sinf(i * (M_PI * 2) / l)});
    }
}

Cone::~Cone() {}

void Cone::draw()
{
    glBegin(m_draw_mode);

    float mod_color = 0;
    for (Vector3f &p : points)
    {
        glColor3f(m_color.x - mod_color, m_color.y - mod_color, m_color.z - mod_color);
        glVertex3f(origin.x, origin.y + 1, origin.z);
        glVertex3f(p.x, p.y, p.z);
        mod_color += 0.01;
    }

    for (Vector3f &p : points)
    {
        glColor3f(m_color.x - mod_color, m_color.y - mod_color, m_color.z - mod_color);
        glVertex3f(origin.x, origin.y, origin.z);
        glVertex3f(p.x, p.y, p.z);
        mod_color += 0.01;
    }

    glEnd();
    glFlush();
}
